﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateAndEvents_hw_1
{
    public class SimpleCalculator
    {
        public int NumberGetter()
        {
            Console.Write("Enter positive number: ");
            int number = int.Parse(Console.ReadLine());
            while(number < 0)
            {
                Console.Write("Try again! Enter positive number: ");
                number = int.Parse(Console.ReadLine());
            }
            return number;
        }

        public void PrintMenu()
        {
            Console.WriteLine("1. +");
            Console.WriteLine("2. -");
            Console.WriteLine("3. *");
            Console.WriteLine("4. /");
        }

        public int GetUserChoice()
        {
            Console.Write("Please pick number between 1-4: ");
            int choice = int.Parse(Console.ReadLine());
            while (choice < 1 && choice > 4)
            {
                Console.Write("Try again! pick number between 1-4: ");
                choice = int.Parse(Console.ReadLine());
            }
            return choice;
        }

        public double Calculate(int num1, int num2, int choice)
        {
            PrintMenu();
            switch (choice)
            {
                case 1: return num1 + num2;
                case 2: return num1 - num2;
                case 3: return num1 * num2;
                case 4: return num1 / num2;
                default:
                    return 0;
            }
        }

        public void PrintResultNicely(double number)
        {
            Console.Write($"result: {number} = ");
            for (int i = 0; i < number; i++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }

    }
}
